define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'comment/index' + location.search,
                    add_url: 'comment/add',
                    edit_url: 'comment/edit',
                    del_url: 'comment/del',
                    multi_url: 'comment/multi',
                    table: 'comment',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'flag', title: __('Flag'), searchList: {"hot":__('Flag hot'),"index":__('Flag index'),"recommend":__('Flag recommend'),"exquisite":__('Flag exquisite'),"place":__('Flag place')}, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'delicious', title: __('Delicious'), searchList: {"1":__('Delicious 1'),"2":__('Delicious 2'),"3":__('Delicious 3'),"4":__('Delicious 4'),"5":__('Delicious 5')}, formatter: Table.api.formatter.normal},
                        {field: 'attitude', title: __('Attitude'), searchList: {"1":__('Attitude 1'),"2":__('Attitude 2'),"3":__('Attitude 3'),"4":__('Attitude 4'),"5":__('Attitude 5')}, formatter: Table.api.formatter.normal},
                        {field: 'health', title: __('Health'), searchList: {"1":__('Health 1'),"2":__('Health 2'),"3":__('Health 3'),"4":__('Health 4'),"5":__('Health 5')}, formatter: Table.api.formatter.normal},
                        {field: 'comments', title: __('Comments')},
                        {field: 'commentimages', title: __('Commentimages'), events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'good_comments', title: __('Good_comments')},
                        {field: 'bad_comment', title: __('Bad_comment')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'), searchList: {"normal":__('Normal'),"hidden":__('Hidden')}, formatter: Table.api.formatter.status},
                        {field: 'user.username', title: __('User.username')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});