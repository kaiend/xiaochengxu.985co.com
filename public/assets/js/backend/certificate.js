define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'certificate/index' + location.search,
                    add_url: 'certificate/add',
                    edit_url: 'certificate/edit',
                    del_url: 'certificate/del',
                    multi_url: 'certificate/multi',
                    table: 'certificate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'conpanyName', title: __('Conpanyname')},
                        {field: 'conpanyLogoavatar', title: __('Conpanylogoavatar'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'testResultlist', title: __('Testresultlist'), searchList: {"A":__('Testresultlist a'),"B":__('Testresultlist b'),"C":__('Testresultlist c')}, formatter: Table.api.formatter.normal},
                        {field: 'resultlist', title: __('Resultlist'), searchList: {"A":__('Resultlist a'),"B":__('Resultlist b'),"C":__('Resultlist c')}, formatter: Table.api.formatter.normal},
                        {field: 'businessAddress', title: __('Businessaddress')},
                        {field: 'legalRepresentative', title: __('Legalrepresentative')},
                        {field: 'permitNumber', title: __('Permitnumber')},
                        {field: 'businessScope', title: __('Businessscope')},
                        {field: 'validityPerioddate', title: __('Validityperioddate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'businessimage', title: __('Businessimage'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'foodimage', title: __('Foodimage'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'cateringserviceimage', title: __('Cateringserviceimage'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'healthimage', title: __('Healthimage'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'bannerimages', title: __('Bannerimages'), events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});