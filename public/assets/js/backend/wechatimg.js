define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'wechatimg/index' + location.search,
                    add_url: 'wechatimg/add',
                    edit_url: 'wechatimg/edit',
                    del_url: 'wechatimg/del',
                    multi_url: 'wechatimg/multi',
                    table: 'wechatimg',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'imgPath', title: __('Imgpath'),formatter: Table.api.formatter.image},
                    	{field: 'operate', title: __('Operate'), table: table, formatter: Table.api.formatter.operate, buttons: [
                                {name: 'detail', text: '下载', title: '下载', icon: 'fa fa-download', /*classname: 'btn btn-xs btn-primary btn-dialog',*/ url: 'qrcode/download_img'}
                            ], // 添加按钮， btn-dialog 新建页面 btn-ajax ajax异步请求
                            events: Table.api.events.operate,}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});