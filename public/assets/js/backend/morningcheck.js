define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'morningcheck/index' + location.search,
                    add_url: 'morningcheck/add',
                    edit_url: 'morningcheck/edit',
                    del_url: 'morningcheck/del',
                    multi_url: 'morningcheck/multi',
                    table: 'morningcheck',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                         {field: 'worker.name', title: __('Worker.name')},
                        {field: 'diseasedata', title: __('Diseasedata'), searchList: {"1":__('Diseasedata 1'),"2":__('Diseasedata 2'),"3":__('Diseasedata 3'),"4":__('Diseasedata 4'),"5":__('Diseasedata 5'),"6":__('Diseasedata 6'),"7":__('Diseasedata 7'),"8":__('Diseasedata 8'),"9":__('Diseasedata 9'),"10":__('Diseasedata 10'),"11":__('Diseasedata 11'),"12":__('Diseasedata 12')}, operate:'FIND_IN_SET', formatter: Table.api.formatter.label},
                        {field: 'startdate', title: __('Startdate'), operate:'RANGE', addclass:'datetimerange'},
                       
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});