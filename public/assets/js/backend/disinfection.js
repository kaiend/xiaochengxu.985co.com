define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'disinfection/index' + location.search,
                    add_url: 'disinfection/add',
                    edit_url: 'disinfection/edit',
                    del_url: 'disinfection/del',
                    multi_url: 'disinfection/multi',
                    table: 'disinfection',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'startdate', title: __('Startdate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'bowl', title: __('Bowl')},
                        {field: 'plate', title: __('Plate')},
                        {field: 'spoon', title: __('Spoon')},
                        {field: 'cup', title: __('Cup')},
                        {field: 'other', title: __('Other')},
                        {field: 'concentration', title: __('Concentration')},
                        {field: 'humidity', title: __('Humidity')},
                        {field: 'more', title: __('More')},
                        {field: 'lasting', title: __('Lasting')},
                        {field: 'principalName', title: __('Principalname')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});