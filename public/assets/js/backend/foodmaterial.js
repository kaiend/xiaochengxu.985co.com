define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'foodmaterial/index' + location.search,
                    add_url: 'foodmaterial/add',
                    edit_url: 'foodmaterial/edit',
                    del_url: 'foodmaterial/del',
                    multi_url: 'foodmaterial/multi',
                    table: 'foodmaterial',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'startdate', title: __('Startdate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'materialName', title: __('Materialname')},
                        {field: 'specification', title: __('Specification')},
                        {field: 'number', title: __('Number')},
                        {field: 'production', title: __('Production')},
                        {field: 'fresh', title: __('Fresh'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'supplierName', title: __('Suppliername')},
                        {field: 'supplierMobile', title: __('Suppliermobile')},
                        {field: 'purchasedate', title: __('Purchasedate'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'accept', title: __('Accept')},
                        {field: 'acceptName', title: __('Acceptname')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});