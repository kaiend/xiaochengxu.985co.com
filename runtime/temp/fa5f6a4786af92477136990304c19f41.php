<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:90:"/www/wwwroot/xiaochengxu.985co.com/public/../application/admin/view/disinfection/edit.html";i:1569389508;s:77:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/layout/default.html";i:1568538039;s:74:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/meta.html";i:1562338656;s:76:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/script.html";i:1562338656;}*/ ?>
<!DOCTYPE html>
<html lang="<?php echo $config['language']; ?>">
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>
    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !$config['fastadmin']['multiplenav']): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Startdate'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-startdate" class="form-control datetimepicker" data-date-format="YYYY-MM-DD" data-use-current="true" name="row[startdate]" type="text" value="<?php echo $row['startdate']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Bowl'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-bowl" class="form-control" name="row[bowl]" type="text" value="<?php echo htmlentities($row['bowl']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Plate'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-plate" class="form-control" name="row[plate]" type="text" value="<?php echo htmlentities($row['plate']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Spoon'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-spoon" class="form-control" name="row[spoon]" type="text" value="<?php echo htmlentities($row['spoon']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Cup'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-cup" class="form-control" name="row[cup]" type="text" value="<?php echo htmlentities($row['cup']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Other'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-other" class="form-control" name="row[other]" type="text" value="<?php echo htmlentities($row['other']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Concentration'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-concentration" class="form-control" name="row[concentration]" type="text" value="<?php echo htmlentities($row['concentration']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Humidity'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-humidity" class="form-control" name="row[humidity]" type="text" value="<?php echo htmlentities($row['humidity']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('More'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-more" class="form-control" name="row[more]" type="text" value="<?php echo htmlentities($row['more']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Lasting'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-lasting" class="form-control" name="row[lasting]" type="text" value="<?php echo htmlentities($row['lasting']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Principalname'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-principalName" class="form-control" name="row[principalName]" type="text" value="<?php echo htmlentities($row['principalName']); ?>">
        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo $site['version']; ?>"></script>
    </body>
</html>