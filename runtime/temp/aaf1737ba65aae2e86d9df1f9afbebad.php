<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:85:"/www/wwwroot/xiaochengxu.985co.com/public/../application/admin/view/comment/edit.html";i:1569591772;s:77:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/layout/default.html";i:1568538039;s:74:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/meta.html";i:1562338656;s:76:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/script.html";i:1562338656;}*/ ?>
<!DOCTYPE html>
<html lang="<?php echo $config['language']; ?>">
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>
    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !$config['fastadmin']['multiplenav']): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('User_id'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-user_id" data-rule="required" data-source="user/user/index" data-field="nickname" class="form-control selectpage" name="row[user_id]" type="text" value="<?php echo htmlentities($row['user_id']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Flag'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-flag" data-rule="required" class="form-control selectpicker" multiple="" name="row[flag][]">
                <?php if(is_array($flagList) || $flagList instanceof \think\Collection || $flagList instanceof \think\Paginator): if( count($flagList)==0 ) : echo "" ;else: foreach($flagList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['flag'])?$row['flag']:explode(',',$row['flag']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Content'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <textarea id="c-content" class="form-control editor" rows="5" name="row[content]" cols="50"><?php echo htmlentities($row['content']); ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Delicious'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-delicious" class="form-control selectpicker" name="row[delicious]">
                <?php if(is_array($deliciousList) || $deliciousList instanceof \think\Collection || $deliciousList instanceof \think\Paginator): if( count($deliciousList)==0 ) : echo "" ;else: foreach($deliciousList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['delicious'])?$row['delicious']:explode(',',$row['delicious']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Attitude'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-attitude" class="form-control selectpicker" name="row[attitude]">
                <?php if(is_array($attitudeList) || $attitudeList instanceof \think\Collection || $attitudeList instanceof \think\Paginator): if( count($attitudeList)==0 ) : echo "" ;else: foreach($attitudeList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['attitude'])?$row['attitude']:explode(',',$row['attitude']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Health'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-health" class="form-control selectpicker" name="row[health]">
                <?php if(is_array($healthList) || $healthList instanceof \think\Collection || $healthList instanceof \think\Paginator): if( count($healthList)==0 ) : echo "" ;else: foreach($healthList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['health'])?$row['health']:explode(',',$row['health']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Comments'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-comments" data-rule="required" class="form-control" name="row[comments]" type="number" value="<?php echo htmlentities($row['comments']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Commentimages'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-commentimages" class="form-control" size="50" name="row[commentimages]" type="text" value="<?php echo htmlentities($row['commentimages']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-commentimages" class="btn btn-danger plupload" data-input-id="c-commentimages" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="true" data-preview-id="p-commentimages"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-commentimages" class="btn btn-primary fachoose" data-input-id="c-commentimages" data-mimetype="image/*" data-multiple="true"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-commentimages"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-commentimages"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Good_comments'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-good_comments" class="form-control" name="row[good_comments]" type="number" value="<?php echo htmlentities($row['good_comments']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Bad_comment'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-bad_comment" class="form-control" name="row[bad_comment]" type="number" value="<?php echo htmlentities($row['bad_comment']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Good_hot'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-good_hot" class="form-control" name="row[good_hot]" type="number" value="<?php echo htmlentities($row['good_hot']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Good_index'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-good_index" class="form-control" name="row[good_index]" type="number" value="<?php echo htmlentities($row['good_index']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Good_recommend'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-good_recommend" class="form-control" name="row[good_recommend]" type="number" value="<?php echo htmlentities($row['good_recommend']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Good_exquisite'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-good_exquisite" class="form-control" name="row[good_exquisite]" type="number" value="<?php echo htmlentities($row['good_exquisite']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Good_place'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-good_place" class="form-control" name="row[good_place]" type="number" value="<?php echo htmlentities($row['good_place']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Status'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            
            <div class="radio">
            <?php if(is_array($statusList) || $statusList instanceof \think\Collection || $statusList instanceof \think\Paginator): if( count($statusList)==0 ) : echo "" ;else: foreach($statusList as $key=>$vo): ?>
            <label for="row[status]-<?php echo $key; ?>"><input id="row[status]-<?php echo $key; ?>" name="row[status]" type="radio" value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['status'])?$row['status']:explode(',',$row['status']))): ?>checked<?php endif; ?> /> <?php echo $vo; ?></label> 
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>

        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo $site['version']; ?>"></script>
    </body>
</html>