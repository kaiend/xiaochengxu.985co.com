<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:90:"/www/wwwroot/xiaochengxu.985co.com/public/../application/admin/view/foodmaterial/edit.html";i:1569407549;s:77:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/layout/default.html";i:1568538039;s:74:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/meta.html";i:1562338656;s:76:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/script.html";i:1562338656;}*/ ?>
<!DOCTYPE html>
<html lang="<?php echo $config['language']; ?>">
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>
    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !$config['fastadmin']['multiplenav']): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Startdate'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-startdate" class="form-control datetimepicker" data-date-format="YYYY-MM-DD" data-use-current="true" name="row[startdate]" type="text" value="<?php echo $row['startdate']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Materialname'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-materialName" class="form-control" name="row[materialName]" type="text" value="<?php echo htmlentities($row['materialName']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Specification'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-specification" class="form-control" name="row[specification]" type="text" value="<?php echo htmlentities($row['specification']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Number'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-number" class="form-control" name="row[number]" type="number" value="<?php echo htmlentities($row['number']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Production'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-production" class="form-control" name="row[production]" type="text" value="<?php echo htmlentities($row['production']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Fresh'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-fresh" class="form-control datetimepicker" data-date-format="YYYY-MM-DD" data-use-current="true" name="row[fresh]" type="text" value="<?php echo $row['fresh']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Suppliername'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-supplierName" class="form-control" name="row[supplierName]" type="text" value="<?php echo htmlentities($row['supplierName']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Suppliermobile'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-supplierMobile" class="form-control" name="row[supplierMobile]" type="text" value="<?php echo htmlentities($row['supplierMobile']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Purchasedate'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-purchasedate" class="form-control datetimepicker" data-date-format="YYYY-MM-DD" data-use-current="true" name="row[purchasedate]" type="text" value="<?php echo $row['purchasedate']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Accept'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-accept" class="form-control" name="row[accept]" type="text" value="<?php echo htmlentities($row['accept']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Acceptname'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-acceptName" class="form-control" name="row[acceptName]" type="text" value="<?php echo htmlentities($row['acceptName']); ?>">
        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo $site['version']; ?>"></script>
    </body>
</html>