<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:95:"/www/wwwroot/xiaochengxu.985co.com/public/../application/admin/view/cwmap/maplocation/edit.html";i:1571982035;s:77:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/layout/default.html";i:1568538039;s:74:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/meta.html";i:1562338656;s:76:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/script.html";i:1562338656;}*/ ?>
<!DOCTYPE html>
<html lang="<?php echo $config['language']; ?>">
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>
    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !$config['fastadmin']['multiplenav']): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Locationname'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-locationname" data-rule="required" class="form-control " name="row[locationname]" type="text" value="<?php echo $row['locationname']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Detailaddress'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-detailaddress" class="form-control " name="row[detailaddress]" type="text" value="<?php echo $row['detailaddress']; ?>">
        </div>
    </div>
    
     <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2">分类:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-assort_ids" data-rule="required" data-source="assort/index" data-multiple="true" data-live-search="true"
                   class="form-control selectpage form-control" name="row[assort_ids]" type="text"
                    value="<?php echo htmlentities($row['assort_ids']); ?>">
        </div>
    </div>
    
    <div class="form-group" style="width:65%;height:250px;margin:0 auto;">
        <div class="col-xs-12 col-sm-8" id='allmap' style="width: 100%;height: 100%;overflow: hidden;margin:0;"></div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Search'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="searchaddress" class="form-control " type="text" placeholder="<?php echo __('Search tips'); ?>">
        </div>
        <div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Longitude'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-longitude" class="form-control " name="row[longitude]" type="text" value="<?php echo $row['longitude']; ?>" readonly="readonly">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Latitude'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-latitude" class="form-control " name="row[latitude]" type="text" value="<?php echo $row['latitude']; ?>" readonly="readonly">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Phone'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-phone" class="form-control " name="row[phone]" type="text" value="<?php echo $row['phone']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Email'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-email" class="form-control " name="row[email]" type="text" value="<?php echo $row['email']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Fax'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-fax" class="form-control " name="row[fax]" type="text" value="<?php echo $row['fax']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('商户id'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-qq" class="form-control " name="row[qq]" type="text" value="<?php echo $row['qq']; ?>" data-rule="required">
        </div>
    </div>
    <div class="form-group">
        <label for="c-picture" class="control-label col-xs-12 col-sm-2"><?php echo __('Picture'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-picture" data-rule="" class="form-control" size="50" name="row[picture]" type="text" value="<?php echo $row['picture']; ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-picture" class="btn btn-danger plupload" data-input-id="c-picture" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="false" data-preview-id="p-picture"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-picture" class="btn btn-primary fachoose" data-input-id="c-picture" data-mimetype="image/*" data-multiple="false"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-picture"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-picture"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Website'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-website" class="form-control " name="row[website]" type="text" value="<?php echo $row['website']; ?>">
        </div>
    </div>    
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Province'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-province" class="form-control " name="row[province]" type="text" value="<?php echo $row['province']; ?>">
        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
    <div id='allmap'></div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo $site['version']; ?>"></script>
    </body>
</html>