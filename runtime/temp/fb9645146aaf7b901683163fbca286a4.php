<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:89:"/www/wwwroot/xiaochengxu.985co.com/public/../application/admin/view/certificate/edit.html";i:1569599801;s:77:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/layout/default.html";i:1568538039;s:74:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/meta.html";i:1562338656;s:76:"/www/wwwroot/xiaochengxu.985co.com/application/admin/view/common/script.html";i:1562338656;}*/ ?>
<!DOCTYPE html>
<html lang="<?php echo $config['language']; ?>">
    <head>
        <meta charset="utf-8">
<title><?php echo (isset($title) && ($title !== '')?$title:''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="renderer" content="webkit">

<link rel="shortcut icon" href="/assets/img/favicon.ico" />
<!-- Loading Bootstrap -->
<link href="/assets/css/backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.css?v=<?php echo \think\Config::get('site.version'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
    var require = {
        config:  <?php echo json_encode($config); ?>
    };
</script>
    </head>

    <body class="inside-header inside-aside <?php echo defined('IS_DIALOG') && IS_DIALOG ? 'is-dialog' : ''; ?>">
        <div id="main" role="main">
            <div class="tab-content tab-addtabs">
                <div id="content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <section class="content-header hide">
                                <h1>
                                    <?php echo __('Dashboard'); ?>
                                    <small><?php echo __('Control panel'); ?></small>
                                </h1>
                            </section>
                            <?php if(!IS_DIALOG && !$config['fastadmin']['multiplenav']): ?>
                            <!-- RIBBON -->
                            <div id="ribbon">
                                <ol class="breadcrumb pull-left">
                                    <li><a href="dashboard" class="addtabsit"><i class="fa fa-dashboard"></i> <?php echo __('Dashboard'); ?></a></li>
                                </ol>
                                <ol class="breadcrumb pull-right">
                                    <?php foreach($breadcrumb as $vo): ?>
                                    <li><a href="javascript:;" data-url="<?php echo $vo['url']; ?>"><?php echo $vo['title']; ?></a></li>
                                    <?php endforeach; ?>
                                </ol>
                            </div>
                            <!-- END RIBBON -->
                            <?php endif; ?>
                            <div class="content">
                                <form id="edit-form" class="form-horizontal" role="form" data-toggle="validator" method="POST" action="">

    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Conpanyname'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-conpanyName" class="form-control" name="row[conpanyName]" type="text" value="<?php echo htmlentities($row['conpanyName']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Conpanylogoavatar'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-conpanyLogoavatar" class="form-control" size="50" name="row[conpanyLogoavatar]" type="text" value="<?php echo htmlentities($row['conpanyLogoavatar']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-conpanyLogoavatar" class="btn btn-danger plupload" data-input-id="c-conpanyLogoavatar" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="false" data-preview-id="p-conpanyLogoavatar"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-conpanyLogoavatar" class="btn btn-primary fachoose" data-input-id="c-conpanyLogoavatar" data-mimetype="image/*" data-multiple="false"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-conpanyLogoavatar"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-conpanyLogoavatar"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Testresultlist'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-testResultlist" class="form-control selectpicker" name="row[testResultlist]">
                <?php if(is_array($testresultlistList) || $testresultlistList instanceof \think\Collection || $testresultlistList instanceof \think\Paginator): if( count($testresultlistList)==0 ) : echo "" ;else: foreach($testresultlistList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['testResultlist'])?$row['testResultlist']:explode(',',$row['testResultlist']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Resultlist'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
                        
            <select  id="c-resultlist" class="form-control selectpicker" name="row[resultlist]">
                <?php if(is_array($resultlistList) || $resultlistList instanceof \think\Collection || $resultlistList instanceof \think\Paginator): if( count($resultlistList)==0 ) : echo "" ;else: foreach($resultlistList as $key=>$vo): ?>
                    <option value="<?php echo $key; ?>" <?php if(in_array(($key), is_array($row['resultlist'])?$row['resultlist']:explode(',',$row['resultlist']))): ?>selected<?php endif; ?>><?php echo $vo; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Businessaddress'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-businessAddress" class="form-control" name="row[businessAddress]" type="text" value="<?php echo htmlentities($row['businessAddress']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Legalrepresentative'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-legalRepresentative" class="form-control" name="row[legalRepresentative]" type="text" value="<?php echo htmlentities($row['legalRepresentative']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Permitnumber'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-permitNumber" class="form-control" name="row[permitNumber]" type="text" value="<?php echo htmlentities($row['permitNumber']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Businessscope'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-businessScope" class="form-control" name="row[businessScope]" type="text" value="<?php echo htmlentities($row['businessScope']); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Validityperioddate'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <input id="c-validityPerioddate" class="form-control datetimepicker" data-date-format="YYYY-MM-DD" data-use-current="true" name="row[validityPerioddate]" type="text" value="<?php echo $row['validityPerioddate']; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Businessimage'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-businessimage" class="form-control" size="50" name="row[businessimage]" type="text" value="<?php echo htmlentities($row['businessimage']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-businessimage" class="btn btn-danger plupload" data-input-id="c-businessimage" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="false" data-preview-id="p-businessimage"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-businessimage" class="btn btn-primary fachoose" data-input-id="c-businessimage" data-mimetype="image/*" data-multiple="false"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-businessimage"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-businessimage"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Foodimage'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-foodimage" class="form-control" size="50" name="row[foodimage]" type="text" value="<?php echo htmlentities($row['foodimage']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-foodimage" class="btn btn-danger plupload" data-input-id="c-foodimage" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="false" data-preview-id="p-foodimage"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-foodimage" class="btn btn-primary fachoose" data-input-id="c-foodimage" data-mimetype="image/*" data-multiple="false"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-foodimage"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-foodimage"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Cateringserviceimage'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-cateringserviceimage" class="form-control" size="50" name="row[cateringserviceimage]" type="text" value="<?php echo htmlentities($row['cateringserviceimage']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-cateringserviceimage" class="btn btn-danger plupload" data-input-id="c-cateringserviceimage" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="false" data-preview-id="p-cateringserviceimage"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-cateringserviceimage" class="btn btn-primary fachoose" data-input-id="c-cateringserviceimage" data-mimetype="image/*" data-multiple="false"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-cateringserviceimage"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-cateringserviceimage"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Healthimage'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-healthimage" class="form-control" size="50" name="row[healthimage]" type="text" value="<?php echo htmlentities($row['healthimage']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-healthimage" class="btn btn-danger plupload" data-input-id="c-healthimage" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="false" data-preview-id="p-healthimage"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-healthimage" class="btn btn-primary fachoose" data-input-id="c-healthimage" data-mimetype="image/*" data-multiple="false"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-healthimage"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-healthimage"></ul>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-xs-12 col-sm-2"><?php echo __('Bannerimages'); ?>:</label>
        <div class="col-xs-12 col-sm-8">
            <div class="input-group">
                <input id="c-bannerimages" class="form-control" size="50" name="row[bannerimages]" type="text" value="<?php echo htmlentities($row['bannerimages']); ?>">
                <div class="input-group-addon no-border no-padding">
                    <span><button type="button" id="plupload-bannerimages" class="btn btn-danger plupload" data-input-id="c-bannerimages" data-mimetype="image/gif,image/jpeg,image/png,image/jpg,image/bmp" data-multiple="true" data-preview-id="p-bannerimages"><i class="fa fa-upload"></i> <?php echo __('Upload'); ?></button></span>
                    <span><button type="button" id="fachoose-bannerimages" class="btn btn-primary fachoose" data-input-id="c-bannerimages" data-mimetype="image/*" data-multiple="true"><i class="fa fa-list"></i> <?php echo __('Choose'); ?></button></span>
                </div>
                <span class="msg-box n-right" for="c-bannerimages"></span>
            </div>
            <ul class="row list-inline plupload-preview" id="p-bannerimages"></ul>
        </div>
    </div>
    <div class="form-group layer-footer">
        <label class="control-label col-xs-12 col-sm-2"></label>
        <div class="col-xs-12 col-sm-8">
            <button type="submit" class="btn btn-success btn-embossed disabled"><?php echo __('OK'); ?></button>
            <button type="reset" class="btn btn-default btn-embossed"><?php echo __('Reset'); ?></button>
        </div>
    </div>
</form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/assets/js/require<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js" data-main="/assets/js/require-backend<?php echo \think\Config::get('app_debug')?'':'.min'; ?>.js?v=<?php echo $site['version']; ?>"></script>
    </body>
</html>