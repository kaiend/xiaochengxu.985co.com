<?php
/**
 * Notes:小程序二维码带参数
 * User: Administrator
 * Date: 2019/9/26
 * Time: 20:30
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;

class Qrcode extends Backend
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    public function qr()
    {
        $admin_id = $this->auth->id;
        header('content-type:text/html;charset=utf-8');
//配置APPID、APPSECRET
        $APPID = "wxcc486620bb48fbe9";
        $APPSECRET = "a577d5cfd879bc825e70d29fb008cebd";
//获取access_token
        $access_token = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$APPID&secret=$APPSECRET";

//缓存access_token
        //session_start();
        $_SESSION['access_token'] = "";
        $_SESSION['expires_in'] = 0;

        $ACCESS_TOKEN = "";
        if (!isset($_SESSION['access_token']) || (isset($_SESSION['expires_in']) && time() > $_SESSION['expires_in'])) {

            $json = httpRequest($access_token);
            $json = json_decode($json, true);
            // var_dump($json);
            $_SESSION['access_token'] = $json['access_token'];
            $_SESSION['expires_in'] = time() + 7200;
            $ACCESS_TOKEN = $json["access_token"];
        } else {

            $ACCESS_TOKEN = $_SESSION["access_token"];
        }

//构建请求二维码参数
//path是扫描二维码跳转的小程序路径，可以带参数?id=xxx
//width是二维码宽度
        $qcode = "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token=$ACCESS_TOKEN";
        $param = json_encode(array("path" => "/viewControllers/Menu/Menu?admin_id=" . $admin_id, "width" => 150));

//POST参数
        $result = httpRequest($qcode, $param, "POST");
        //halt($result);
//生成二维码
        // file_put_contents("qrcode.png", $result);

        $image = "data:image/jpeg;base64," . base64_encode($result);//返回字节

        $imageName = "25220_" . date("His", time()) . "_" . rand(1111, 9999) . '.png';
        if (strstr($image, ",")) {
            $image = explode(',', $image);
            $image = $image[1];
        }
        $path = ROOT_PATH . 'public/uploads/' . date("Ymd", time());

        if (!is_dir($path)) { //判断目录是否存在 不存在就创建
            mkdir($path, 0777, true);
        }
        $imageSrc = $path . "/" . $imageName; //图片名字
        $r = file_put_contents($imageSrc, base64_decode($image));//返回的是字节数
        //halt($imageSrc);
        $data = ['admin_id' => $admin_id, 'imgPath' => $imageSrc, 'createtime' => time()];
        Db::name('wechatimg')->insert($data);
        /**
         * 下载图片
         */

    }

    /**
     * 下载二维码图片
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function download_img()
    {
        $admin_id = $this->auth->id;
        $imageSrc = Db::name('wechatimg')->where('admin_id', $admin_id)->find();
        $length = filesize($imageSrc['imgPath']);
        //$type = mime_content_type($imageSrc);
        $showname = ltrim(strrchr($imageSrc['imgPath'], '/'), '/');
        header("Content-Description: File Transfer");
        // header('Content-type: ' . $type);
        header('Content-Length:' . $length);
        if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
            header('Content-Disposition: attachment; filename="' . rawurlencode($showname) . '"');
        } else {
            header('Content-Disposition: attachment; filename="' . $showname . '"');
        }
        readfile($imageSrc['imgPath']);

    }

}