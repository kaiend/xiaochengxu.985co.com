<?php

return [
    'Id'             => 'id',
    'Name'           => '品类名称',
    'Admin_id'       => '关联admin表id',
    'State'          => '状态值',
    'State 0'        => '下架',
    'State 1'        => '上架',
    'Admin.username' => '用户名'
];
