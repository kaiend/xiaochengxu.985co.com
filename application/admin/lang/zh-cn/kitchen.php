<?php

return [
    'Id'              => 'id',
    'Startdate'       => '创建时间',
    'Title'           => '种类',
    'Weight'          => '数量(kg)',
    'Receivecopyname' => '接收单位',
    'Address'         => '详细地址',
    'Usefulness'      => '用途',
    'Mobile'          => '联系电话',
    'Attendantname'   => '餐饮服务人员(签名)',
    'Receivingname'   => '接收人员(签名)',
    'Admin_id'        => '关联商户id'
];
