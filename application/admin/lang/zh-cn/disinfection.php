<?php

return [
    'Id'            => 'id',
    'Startdate'     => '日期',
    'Bowl'          => '碗',
    'Plate'         => '碟子',
    'Spoon'         => '汤匙',
    'Cup'           => '杯子',
    'Other'         => '其他',
    'Concentration' => '药物浓度(化学)',
    'Humidity'      => '消毒湿度(热力)',
    'More'          => '其他方法',
    'Lasting'       => '消毒时间',
    'Principalname' => '负责人',
    'Admin_id'      => '管理员ID'
];
