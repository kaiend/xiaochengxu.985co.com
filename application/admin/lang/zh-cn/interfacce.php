<?php

return [
    'Id'          => 'ID',
    'Name'        => '接口名称',
    'State'       => '状态',
    'State 1'     => '显示',
    'State 2'     => '关闭',
    'Refreshtime' => '更新时间(int)'
];
