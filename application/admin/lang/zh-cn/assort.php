<?php

return [
    'Id'          => 'ID',
    'Name'        => '商户分类名称',
    'State'       => '状态',
    'State 0'     => '禁用',
    'State 1'     => '正常',
    'Description' => '备注',
    'Createtime'  => '创建时间'
];
