<?php

return [
    'Id'             => 'ID',
    'Admin_id'       => '管理员ID',
    'Startdate'      => '日期',
    'Materialname'   => '产品名称',
    'Specification'  => '规格',
    'Number'         => '数量',
    'Production'     => '生产批次',
    'Fresh'          => '保质期',
    'Suppliername'   => '供货商名称',
    'Suppliermobile' => '供货商电话',
    'Purchasedate'   => '进货日期',
    'Accept'         => '验收结论',
    'Acceptname'     => '验收人'
];
