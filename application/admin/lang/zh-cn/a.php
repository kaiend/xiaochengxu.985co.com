<?php

return [
    'Id'         => 'id',
    'Name'       => '接口名称',
    'State'      => '状态值',
    'State 1'    => '正常',
    'State 2'    => '关闭',
    'Admin_id'   => '管理员ID',
    'Updatetime' => '更新时间'
];
