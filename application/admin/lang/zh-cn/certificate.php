<?php

return [
    'Id'                   => 'id',
    'Admin_id'             => '关联admin表的id',
    'Conpanyname'          => '单位名称',
    'Conpanylogoavatar'    => 'logo',
    'Testresultlist'       => '上年度综合等级',
    'Testresultlist a'     => 'A级优秀',
    'Testresultlist b'     => 'B级良好',
    'Testresultlist c'     => 'C级一般',
    'Resultlist'           => '本次检查动态等级',
    'Resultlist a'         => 'A级优秀',
    'Resultlist b'         => 'B级良好',
    'Resultlist c'         => 'C级一般',
    'Businessaddress'      => '经营地址',
    'Legalrepresentative'  => '法定代表人',
    'Permitnumber'         => '许可证号',
    'Businessscope'        => '经营范围',
    'Validityperioddate'   => '有效期',
    'Businessimage'        => '经营许可证',
    'Foodimage'            => '食品流通许可证',
    'Cateringserviceimage' => '餐饮服务许可证',
    'Healthimage'          => '卫生许可证',
    'Bannerimages'         => 'banner'
];
