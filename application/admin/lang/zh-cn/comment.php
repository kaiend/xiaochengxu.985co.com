<?php

return [
    'Id'             => 'ID',
    'User_id'        => '会员ID',
    'Admin_id'       => '关联店ID',
    'Flag'           => '评论标签',
    'Flag hot'       => '环境优雅',
    'Flag index'     => '服务优雅',
    'Flag recommend' => '性价比高',
    'Flag exquisite' => '菜品精致',
    'Flag place'     => '位置好找',
    'Content'        => '评论内容',
    'Delicious'      => '美味口感',
    'Delicious 1'    => '1星',
    'Delicious 2'    => '2星',
    'Delicious 3'    => '3星',
    'Delicious 4'    => '4星',
    'Delicious 5'    => '5星',
    'Attitude'       => '服务态度',
    'Attitude 1'     => '1星',
    'Attitude 2'     => '2星',
    'Attitude 3'     => '3星',
    'Attitude 4'     => '4星',
    'Attitude 5'     => '5星',
    'Health'         => '环境卫生',
    'Health 1'       => '1星',
    'Health 2'       => '2星',
    'Health 3'       => '3星',
    'Health 4'       => '4星',
    'Health 5'       => '5星',
    'Comments'       => '评论数',
    'Commentimages'  => '评论图片',
    'Good_comments'  => '好评数量',
    'Bad_comment'    => '差评数量',
    'Good_hot'       => '环境优雅数量',
    'Good_index'     => '服务优雅数量',
    'Good_recommend' => '性价比高数量',
    'Good_exquisite' => '菜品精致数量',
    'Good_place'     => '位置好找数量',
    'Createtime'     => '创建时间',
    'Updatetime'     => '更新时间',
    'Status'         => '状态',
    'User.username'  => '用户名'
];
