<?php

return [
    'Id'             => 'id',
    'Admin_id'       => 'admin_id',
    'Food_name'      => '菜品名称',
    'New_price'      => '折扣价',
    'Old_price'      => '原价',
    'Posterimage'    => '菜品图片',
    'Kind_id'        => '分类',
    'Ingredientjson' => '材料',
    'State'          => '状态',
    'State 0'        => '下架',
    'State 1'        => '上架',
    'Admin.username' => '用户名'
];
