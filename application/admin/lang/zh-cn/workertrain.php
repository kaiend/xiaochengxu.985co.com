<?php

return [
    'Id'          => 'ID',
    'Admin_id'    => '管理员ID',
    'Startdate'   => '日期',
    'Content'     => '内容',
    'Worker_ids'  => '参加人员',
    'More'        => '备注',
    'Worker.name' => '员工姓名'
];
