<?php

return [
    'Id'           => 'id',
    'State'        => '设为管理人员',
    'State 0'      => '不是',
    'State 1'      => '是',
    'Admin_id'     => '关联商户id',
    'Name'         => '员工姓名',
    'Image'        => '照片',
    'Age'          => '年龄',
    'Card'         => '身份证',
    'Mobil'        => '手机号',
    'Position'     => '职位',
    'Servicenum'   => '工龄',
    'Deadlinedate' => '健康证有效期',
    'Healthimages' => '健康证附件'
];
