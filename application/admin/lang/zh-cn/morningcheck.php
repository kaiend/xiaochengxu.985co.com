<?php

return [
    'Id'             => '序号',
    'Diseasedata'    => '疾病',
    'Diseasedata 1'  => '发热',
    'Diseasedata 2'  => '恶心',
    'Diseasedata 3'  => '呕吐',
    'Diseasedata 4'  => '腹泻',
    'Diseasedata 5'  => '腹痛',
    'Diseasedata 6'  => '外伤',
    'Diseasedata 7'  => '烫伤',
    'Diseasedata 8'  => '湿疹',
    'Diseasedata 9'  => '黄疸',
    'Diseasedata 10' => '咽痛',
    'Diseasedata 11' => '咳嗽',
    'Diseasedata 12' => '其他',
    'Startdate'      => '创建时间',
    'Admin_id'       => '管理员ID',
    'Worker_id'      => '会员ID',
    'Worker.name'    => '员工姓名'
];
