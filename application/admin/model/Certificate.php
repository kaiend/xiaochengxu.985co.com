<?php

namespace app\admin\model;

use think\Model;


class Certificate extends Model
{

    

    

    // 表名
    protected $name = 'certificate';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'testResultlist_text',
        'resultlist_text'
    ];
    

    
    public function getTestresultlistList()
    {
        return ['A' => __('Testresultlist a'), 'B' => __('Testresultlist b'), 'C' => __('Testresultlist c')];
    }

    public function getResultlistList()
    {
        return ['A' => __('Resultlist a'), 'B' => __('Resultlist b'), 'C' => __('Resultlist c')];
    }


    public function getTestresultlistTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['testResultlist']) ? $data['testResultlist'] : '');
        $list = $this->getTestresultlistList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getResultlistTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['resultlist']) ? $data['resultlist'] : '');
        $list = $this->getResultlistList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
