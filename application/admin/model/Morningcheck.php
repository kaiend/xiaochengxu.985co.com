<?php

namespace app\admin\model;

use think\Model;


class Morningcheck extends Model
{

    

    

    // 表名
    protected $name = 'morningcheck';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'diseasedata_text'
    ];
    

    
    public function getDiseasedataList()
    {
        return ['1' => __('Diseasedata 1'), '2' => __('Diseasedata 2'), '3' => __('Diseasedata 3'), '4' => __('Diseasedata 4'), '5' => __('Diseasedata 5'), '6' => __('Diseasedata 6'), '7' => __('Diseasedata 7'), '8' => __('Diseasedata 8'), '9' => __('Diseasedata 9'), '10' => __('Diseasedata 10'), '11' => __('Diseasedata 11'), '12' => __('Diseasedata 12')];
    }


    public function getDiseasedataTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['diseasedata']) ? $data['diseasedata'] : '');
        $valueArr = explode(',', $value);
        $list = $this->getDiseasedataList();
        return implode(',', array_intersect_key($list, array_flip($valueArr)));
    }

    protected function setDiseasedataAttr($value)
    {
        return is_array($value) ? implode(',', $value) : $value;
    }


    public function worker()
    {
        return $this->belongsTo('Worker', 'worker_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
