<?php

namespace app\admin\model;

use think\Model;


class Comment extends Model
{

    

    

    // 表名
    protected $name = 'comment';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'flag_text',
        'delicious_text',
        'attitude_text',
        'health_text',
        'status_text'
    ];
    

    
    public function getFlagList()
    {
        return ['hot' => __('Flag hot'), 'index' => __('Flag index'), 'recommend' => __('Flag recommend'), 'exquisite' => __('Flag exquisite'), 'place' => __('Flag place')];
    }

    public function getDeliciousList()
    {
        return ['1' => __('Delicious 1'), '2' => __('Delicious 2'), '3' => __('Delicious 3'), '4' => __('Delicious 4'), '5' => __('Delicious 5')];
    }

    public function getAttitudeList()
    {
        return ['1' => __('Attitude 1'), '2' => __('Attitude 2'), '3' => __('Attitude 3'), '4' => __('Attitude 4'), '5' => __('Attitude 5')];
    }

    public function getHealthList()
    {
        return ['1' => __('Health 1'), '2' => __('Health 2'), '3' => __('Health 3'), '4' => __('Health 4'), '5' => __('Health 5')];
    }

    public function getStatusList()
    {
        return ['normal' => __('Normal'), 'hidden' => __('Hidden')];
    }


    public function getFlagTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['flag']) ? $data['flag'] : '');
        $valueArr = explode(',', $value);
        $list = $this->getFlagList();
        return implode(',', array_intersect_key($list, array_flip($valueArr)));
    }


    public function getDeliciousTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['delicious']) ? $data['delicious'] : '');
        $list = $this->getDeliciousList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getAttitudeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['attitude']) ? $data['attitude'] : '');
        $list = $this->getAttitudeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getHealthTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['health']) ? $data['health'] : '');
        $list = $this->getHealthList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function setFlagAttr($value)
    {
        return is_array($value) ? implode(',', $value) : $value;
    }


    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
