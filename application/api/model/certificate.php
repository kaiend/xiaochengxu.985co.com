<?php
/**
 * Notes:
 * User: Administrator
 * Date: 2019/9/22
 * Time: 15:35
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */
namespace app\api\model;

use think\Model;
class certificate extends Model
{

    // 表名
    protected $name = 'certificate';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    public function certificate()
    {
        return $this->hasOne('certificate')->setEagerlyType(0);;
    }

}