<?php
/**
 * Notes:
 * User: Administrator
 * Date: 2019/9/28
 * Time: 16:10
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\api\controller;

use think\Db;
use app\common\controller\Api;

class Interfacecontrol extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function inter()
    {
        $inter = Db::name('a')->select();
        $this->success("数据返回成功!", ['inter' => $inter]);
    }

}