<?php
/**
 * Created by PhpStorm.
 * User: kai
 * Date: 2019/10/17
 * Time: 15:57
 */

namespace app\api\controller;

use think\Db;
use app\common\controller\Api;

class Assort extends Api
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     *20191017
     * 小程序页面搜索接口的分类数据
     */
    public function index()
    {
        $assort = Db::name('assort')->selectOrFail();
        $this->success("数据返回成功!", ['resultall' => $assort]);
    }


}