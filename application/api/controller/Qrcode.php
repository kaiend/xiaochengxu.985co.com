<?php
/**
 * Notes:
 * User: Administrator
 * Date: 2019/9/16
 * Time: 21:20
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\api\controller;

use think\Db;
use app\common\controller\Api;

class Qrcode extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function getWxAccessToken()
    {
        $appid = 'wxbcfaa005da7b16**';
        $appsecret = '****';
        if (Session::get('access_token_' . $appid) && Session::get('expire_time_' . $appid) > time()) {
            return Session::get('access_token_' . $appid);
        } else {
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $appid . "&secret=" . $appsecret;
            $access_token = $this->makeRequest($url);
            $access_token = json_decode($access_token['result'], true);
            Session::set('access_token_' . $appid, $access_token);
            Session::set('expire_time_' . $appid, time() + 7000);
            return $access_token;
        }
    }

    /**
     * 发起http请求
     * @param string $url 访问路径
     * @param array $params 参数，该数组多于1个，表示为POST
     * @param int $expire 请求超时时间
     * @param array $extend 请求伪造包头参数
     * @param string $hostIp HOST的地址
     * @return array    返回的为一个请求状态，一个内容
     */
    public function makeRequest($url, $params = array(), $expire = 0, $extend = array(), $hostIp = '')
    {
        if (empty($url)) {
            return array('code' => '100');
        }

        $_curl = curl_init();
        $_header = array(
            'Accept-Language: zh-CN',
            'Connection: Keep-Alive',
            'Cache-Control: no-cache'
        );
        // 方便直接访问要设置host的地址
        if (!empty($hostIp)) {
            $urlInfo = parse_url($url);
            if (empty($urlInfo['host'])) {
                $urlInfo['host'] = substr(DOMAIN, 7, -1);
                $url = "http://{$hostIp}{$url}";
            } else {
                $url = str_replace($urlInfo['host'], $hostIp, $url);
            }
            $_header[] = "Host: {$urlInfo['host']}";
        }

        // 只要第二个参数传了值之后，就是POST的
        if (!empty($params)) {
            curl_setopt($_curl, CURLOPT_POSTFIELDS, http_build_query($params));
            curl_setopt($_curl, CURLOPT_POST, true);
        }

        if (substr($url, 0, 8) == 'https://') {
            curl_setopt($_curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($_curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
        curl_setopt($_curl, CURLOPT_URL, $url);
        curl_setopt($_curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($_curl, CURLOPT_USERAGENT, 'API PHP CURL');
        curl_setopt($_curl, CURLOPT_HTTPHEADER, $_header);

        if ($expire > 0) {
            curl_setopt($_curl, CURLOPT_TIMEOUT, $expire); // 处理超时时间
            curl_setopt($_curl, CURLOPT_CONNECTTIMEOUT, $expire); // 建立连接超时时间
        }

        // 额外的配置
        if (!empty($extend)) {
            curl_setopt_array($_curl, $extend);
        }

        $result['result'] = curl_exec($_curl);
        $result['code'] = curl_getinfo($_curl, CURLINFO_HTTP_CODE);
        $result['info'] = curl_getinfo($_curl);
        if ($result['result'] === false) {
            $result['result'] = curl_error($_curl);
            $result['code'] = -curl_errno($_curl);
        }

        curl_close($_curl);
        return $result;
    }


    public function getWxcode()
    {
        $ACCESS_TOKEN = $this->getWxAccessToken();
        $url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=" . $ACCESS_TOKEN['access_token'];
        $post_data =
            array(
                'page' => 'pages/caregory/index',
                'scene' => '34,S853EE4QRP'//34%2CS853EE4QRP
            );
        $post_data = json_encode($post_data);
        $data = $this->send_post($url, $post_data);
        $result = $this->data_uri($data, 'image/png');
        return '<image src=' . $result . '></image>';
    }

    /**
     * 消息推送http
     * @param $url
     * @param $post_data
     * @return bool|string
     */
    protected function send_post($url, $post_data)
    {
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/json',
                //header 需要设置为 JSON
                'content' => $post_data,
                'timeout' => 60
                //超时时间
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

//二进制转图片image/png
    public function data_uri($contents, $mime)
    {
        $base64 = base64_encode($contents);
        return ('data:' . $mime . ';base64,' . $base64);
    }


    public function send_curl($posts_data)
    {
        //初始化
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, 'https://videossl.985co.com/webapi/login');
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);//0不取得返回头信息 1获得返回头信心
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);
        //设置post数据

        curl_setopt($curl, CURLOPT_POSTFIELDS, $posts_data);
        //执行命令
        $data = curl_exec($curl);
        //关闭URL请求
        curl_close($curl);
        //显示获得的数据
        $myjson = json_decode($data, true);
        return $myjson['tk'];
    }


    public function get_curl()
    {
        $data['user'] = 'admin';
        $data['password'] = 'Admin123';
        $data['force'] = 0;
        $data['login_mode'] = 0;
        $posts_data = \GuzzleHttp\json_encode($data);
//halt($posts_data);
        $tk = $this->send_curl($posts_data);
        // halt($tk);
        $url = 'https://videossl.985co.com/webapi/v4/preview/view?tk=';
        $geturl = $url . $tk;

        //halt($geturl);
        $date = array("chan_serial" => "M030F-A0038199Q04:1");
        $data_string = json_encode($date);
// halt($data_string);
        $res = senUrls($geturl, $data_string);
        $zaotai = json_decode($res);
        $chuyu = json_decode($this->chuyu($posts_data));
        $qingjie = json_decode($this->qingjie($posts_data));
        $qiecai = json_decode($this->qiecai($posts_data));
        $this->success("数据返回成功!", ['zaotai' => $zaotai, 'chuyu' => $chuyu, 'qingjie' => $qingjie, 'qiecai' => $qiecai]);
        //return $res;

    }

    /**
     * 2019 1030
     * 切菜监控视频连接
     * @param $posts_data
     * @return mixed
     */
    public function qiecai($posts_data)
    {
        $tk = $this->send_curl($posts_data);
        $url = 'https://videossl.985co.com/webapi/v4/preview/view?tk=';
        $geturl = $url . $tk;
        $date = array("chan_serial" => "M030F-A0038199Q07:2");
        $data_string = json_encode($date);
        $qiecai = senUrls($geturl, $data_string);
        return $qiecai;

    }

    /**
     * 2019 1030
     * chuyu视频连接
     * @param $posts_data
     * @return mixed
     */
    public function chuyu($posts_data)
    {
        $tk = $this->send_curl($posts_data);
        $url = 'https://videossl.985co.com/webapi/v4/preview/view?tk=';
        $geturl = $url . $tk;
        $date = array("chan_serial" => "M030F-A0038199Q01:1");
        $data_string = json_encode($date);
        $chuyu = senUrls($geturl, $data_string);
        return $chuyu;

    }

    /**
     * 20191030
     * 清洁视频连接
     * @param $posts_data
     * @return mixed
     */
    public function qingjie($posts_data)
    {
        $tk = $this->send_curl($posts_data);
        $url = 'https://videossl.985co.com/webapi/v4/preview/view?tk=';
        $geturl = $url . $tk;
        $date = array("chan_serial" => "M030F-A0038199Q04:2");
        $data_string = json_encode($date);
        $qingjie = senUrls($geturl, $data_string);
        return $qingjie;

    }
    
    /**
     * 还差是否在线没post成功
     **/
     public function get_online()
    {
        $data['user'] = 'admin';
        $data['password'] = 'Admin123';
        $data['force'] = 0;
        $data['login_mode'] = 0;
        $posts_data = \GuzzleHttp\json_encode($data);
//halt($posts_data);
        $tk = $this->send_curl($posts_data);
        // halt($tk);
        $url = 'https://videossl.985co.com/sdk/v4/device/online?tk=';
        $geturl = $url . $tk;

        // halt($geturl);
        // $date = array("dev_serial_list" => ['M030F-A0038199Q04:1','M030F-A0038199Q08:3','M030F-A0038199Q04:1']);
        // $date=['M030F-A0038199Q04','M030F-A0038199Q08','M030F-A0038199Q04'];
          $date = array("dev_serial_list" => ["M030F-A0038199Q04"]);
         $data_string = json_encode($date);
 //halt($data_string);
 $res=curl_post($url,$data_string);
       // $res = senUrls($geturl,$data_string);
        $online = json_decode($res);
        $this->success("数据返回成功!", ['online' => $online]);
        //return $res;

    }
    
    


}