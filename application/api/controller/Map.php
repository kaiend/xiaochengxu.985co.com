<?php
/**
 * Notes:后台地图管理类
 * User: Administrator
 * Date: 2019/10/28
 * Time: 15:17
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\api\controller;

use app\common\controller\Backend;
use think\Db;
use app\common\controller\Api;

class Map extends Api
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    /**
     *
     * 2019 1108
     * sunkai
     * 传参用户 admin_id
     * @param $admin_id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
   /* public function detail($admin_id)
    {

        $map = ['admin_id' => $admin_id];
        $where = ['a.qq' => $admin_id];
        $field = [
            'a.id',
            'a.longitude',
            'a.latitude',
            'a.qq',
            'b.businessAddress',
            'b.legalRepresentative',
            'b.permitNumber',
            'b.businessScope',
            'b.validityPerioddate',
            'b.conpanyName',
            'b.admin_id',
            'b.conpanyLogoavatar'
        ];

        $subsql = DB::name('certificate')
            ->field('admin_id,businessAddress,legalRepresentative,permitNumber,businessScope,validityPerioddate,conpanyName,admin_id,conpanyLogoavatar')
            ->where($map)
            ->buildSql();

        $select = Db::name('cwmap_location');
        $result = $select->alias('a')
            ->where($where)
            ->join([$subsql => 'b'], 'a.qq = b.admin_id', 'left')
            ->field($field)
            ->cache(true, 86400 * 7)
            ->select();
        $this->success("数据返回成功!", ['url' => $result]);
    }*/


    public function index()
    {
        $field = [
            'a.id',
            'a.longitude',
            'a.latitude',
            'a.qq',
            'b.businessAddress',
            'b.legalRepresentative',
            'b.permitNumber',
            'b.businessScope',
            'b.validityPerioddate',
            'b.conpanyName',
            'b.admin_id',
            'b.conpanyLogoavatar'
        ];

        $subsql = DB::name('certificate')
            ->field('admin_id,businessAddress,legalRepresentative,permitNumber,businessScope,validityPerioddate,conpanyName,admin_id,conpanyLogoavatar')
            ->buildSql();

        $select = Db::name('cwmap_location');
        $result = $select->alias('a')
            ->join([$subsql => 'b'], 'a.qq = b.admin_id', 'left')
            ->field($field)
           // ->cache(true, 86400 * 7)
            ->select();
        $this->success("数据返回成功!", ['url' => $result]);
    }

}
