<?php
/**
 * Notes:
 * User: Administrator
 * Date: 2019/9/2
 * Time: 21:51
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\api\controller;

use think\Db;
use app\common\controller\Api;

class Kind extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    public function index($paramID)
    {
        $Menu = action('Store/Index', $paramID);

        //查询数据
        $kind = Db::name('kind')->where(['k_off' => 1])->field('id,k_name')->select()->toArray();
        foreach ($kind as $key => $val) {
            //分类下的食品
            $val['dish'] = Db::name('food')
                ->field('id as aid,food_name,food_img')
                ->where(['food_uid' => $paramID, 'food_pid' => $val['id']])
                ->select()->toArray();
            $kind[$key] = $val;
        }

        $this->success("数据返回成功!", ['Menu' => $Menu, 'Kind' => $kind]);
    }

    public function read($aid)
    {

        $select = Db::name('food');
        $result = $select->where(['id' => $aid])
            ->find();
        $ingredients['id'] = $result['id'];
        $ingredients['name'] = $result['food_name'];
        $ingredients['poster'] = $result['food_img'];
        $ingredients['ingredients'] = $result['ingredients'];
        if (!empty($result) || $result == !null) {
            $this->success("数据返回成功!", ['DishDetail' => $ingredients]);
        }

    }
}