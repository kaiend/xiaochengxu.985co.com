<?php
/**
 * Notes:
 * User: Administrator
 * Date: 2019/9/23
 * Time: 23:27
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\api\controller;

use think\Db;
use app\common\controller\Api;
use think\Config;
use think\Image;
use think\Request;
use Qiniu\Auth as Auth;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;
class Upload extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';
    public function _initialize()
    {
        parent::_initialize();
    }

    /**api图片上传接口
     * @return \think\response\Json|\think\response\View
     * @throws \Exception
     */
    public function upload()
    {
        if (request()->isPost()) {
            $file = request()->file('file');
            // halt($file);
            // 要上传图片的本地路径
            $filePath = $file->getRealPath();
            $ext = pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);  //后缀

            // 上传到七牛后保存的文件名
            $key = substr(md5($file->getRealPath()), 0, 5) . date('YmdHis') . rand(0, 9999) . '.' . $ext;
            require_once ROOT_PATH . 'vendor/php-sdk-master/autoload.php';
            // 需要填写你的 Access Key 和 Secret Key
            $accessKey = config("qiniu.ACCESSKEY");
            $secretKey = config("qiniu.SECRETKEY");
            // 构建鉴权对象
            $auth = new Auth($accessKey, $secretKey);
            // 要上传的空间
            $bucket = config("qiniu.BUCKET");
            $domain = config("qiniu.DOMAINImage");
            $token = $auth->uploadToken($bucket);
            // 初始化 UploadManager 对象并进行文件的上传
            $uploadMgr = new UploadManager();
            // 调用 UploadManager 的 putFile 方法进行文件的上传
            list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);
            // halt($domain);
            if ($err !== null) {
                return json(["err" => 1, "msg" => $err, "data" => ""]);
                die;
            } else {
            	 $this->success("数据返回成功!", ['data' => ($domain . $ret['key'])]);
                //返回图片的完整URL
              
            }
        }
        //return view();
    }

}