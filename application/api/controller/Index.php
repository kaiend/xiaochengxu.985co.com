<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页明厨按钮页面替换 1显示2不显示
     *
     */
    public function index()
    {
        $this->success("数据返回成功!", ['status' => 2, 'address' => 'https://video.985co.com']);//bar页面控制临时使用小程序审核过后就可以废了他，
    }

    /**
     * 小程序首页的信息这个没用了
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function indextest()
    {
        $indexall = Db::name('cwmap_location')
            ->field('id,locationname,picture,longitude,latitude')
            //->where('id', '<>', 1)
            ->select();
        $this->success("数据返回成功!", ['indexall' => $indexall]);
    }

    /**
     * /lng代表经度，lat代表纬度 km默认5KM范围内的
     *
     */
    public function indexall($km = 500000000)
    {//lng代表经度，lat代表纬度
        $slat = $this->request->param('lat');
        $slng = $this->request->param('lng');
        $shop_list = baiduMap($slat, $slng, $km);
        $this->success("数据返回成功!", ['indexall' => $shop_list]);
    }

    /**
     * @param int $km 查询的距离条件
     *  $order  排序条件
     *  $relevance  店铺名称
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */

    public function selectcltext()
    {
        $page = $this->request->param('page');                                    //分页
        $relevance = $this->request->param('locationname');  //店家名字and模糊搜索
        // $name = $this->request->param('name');//模糊搜索
        $lng = $this->request->param('lng');
        $lat = $this->request->param('lat');
        $assort = $this->request->param('assort');//分类
        //halt($assort);
        $orderKsort = $this->request->param('orderKsort');//距离排序
        $order = $this->request->param('good_comments');//好评
        $m = $this->request->param('km');//搜索范围默认5KM，如果为-1那就返回全部数据
        if ($m == '-1') {
            $km = '5000000000000';
        } else {
            $km = $m * 1.0;
        }
        $point = $this->returnSquarePoint($lng, $lat, $km);        //得到四个点

        // tp5的判断大小经度纬度的判断
        $where['latitude'] = array(array('gt', $point['minLat']), array('lt', $point['maxLat']));
        $where['longitude'] = array(array('gt', $point['minLon']), array('lt', $point['maxLon']));

        $subsql = DB::name('comment')->field('admin_id as bid,good_comments')->buildSql();
        $testresult = DB::name('certificate')->field('admin_id as cid,testResultlist')->buildSql();
        // $assortresult = DB::name('assort')->field('admin_id as cid,testResultlist')->buildSql();

        $select = DB::name('cwmap_location');
        $res['count'] = $select->where($where)->count();
        if ($assort == -1) {
            $res['da'] = $select->alias('a')->distinct(true)
                ->whereLike('locationname', "%{$relevance}%")
                ->where($where)
                ->join([$subsql => 'b'], 'a.qq = b.bid', 'left')
                ->join([$testresult => 'c'], 'a.qq = c.cid', 'left')
                ->field('a.id,a.locationname,a.detailaddress,a.longitude,a.latitude,a.picture,a.qq as admin_id,a.assort_ids,b.good_comments,c.testResultlist')
                ->limit(200)
                ->page($page)
                ->group('a.id')
                ->select();
        } else {
            $res['da'] = $select->alias('a')
                ->whereLike('locationname', "%{$relevance}%")
                ->where('assort_ids', $assort)
                ->where($where)
                ->join([$subsql => 'b'], 'a.qq = b.bid', 'left')
                ->join([$testresult => 'c'], 'a.qq = c.cid', 'left')
                ->field('a.id,a.locationname,a.detailaddress,a.longitude,a.latitude,a.picture,a.qq as admin_id,a.assort_ids,b.good_comments,c.testResultlist')
                ->limit(200)
                ->page($page)
                ->distinct(true)
                 ->group('a.id')
                ->select();
        }

        //查询距离的方法调用
        if ($res['count'] > 0) {
            $result = $res['da'];
            foreach ($result as $key => $value) {
                $distance = getDistancem($lat, $value['latitude'], $lng, $value['longitude'], 2, 2);
                $result[$key]['distance'] = round($distance, 0);
            }
            $last_names = array_column($result, 'distance');
            array_multisort($last_names, SORT_ASC, $result);
            $this->success("数据返回成功!", ['indexall' => $result]);
        } else {
            $this->error("暂无数据!");
        }
    }
    // *@param lng float 经度
    //  *@param lat float 纬度
    //  *@param distance float 该点所在圆的半径，该圆与此正方形内切，默认值为单位米
    //  *@return array 正方形的四个点的经纬度坐标
    public function returnSquarePoint($lng, $lat, $distance)
    {
        $PI = 3.14159265;
        $longitude = $lng;
        $latitude = $lat;

        $degree = (24901 * 1609) / 360.0;
        $raidusMile = $distance;

        $dpmLat = 1 / $degree;
        $radiusLat = $dpmLat * $raidusMile;
        $minLat = $latitude - $radiusLat;       //拿到最小纬度
        $maxLat = $latitude + $radiusLat;       //拿到最大纬度

        $mpdLng = $degree * cos($latitude * ($PI / 180));
        $dpmLng = 1 / $mpdLng;
        $radiusLng = $dpmLng * $raidusMile;
        $minLng = $longitude - $radiusLng;     //拿到最小经度
        $maxLng = $longitude + $radiusLng;     //拿到最大经度
        $range = array(
            'minLat' => $minLat,
            'maxLat' => $maxLat,
            'minLon' => $minLng,
            'maxLon' => $maxLng
        );
        return $range;
    }


    /**
     * 点击门店小程序使用
     * @param $admin_id
     * @param int $km
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */


    public function search($admin_id, $km = 500000000000)
    {
        $lng = $this->request->param('lng');
        $lat = $this->request->param('lat');
        $point = $this->returnSquarePoint($lng, $lat, $km);
        // halt($point);
        //得到四个点

        // tp5的判断大小经度纬度的判断
        $where['qq'] = $admin_id;

        $where['latitude'] = array(array('gt', $point['minLat']), array('lt', $point['maxLat']));
        $where['longitude'] = array(array('gt', $point['minLon']), array('lt', $point['maxLon']));

//halt($where);
        $subsql = DB::name('comment')->field('admin_id as bid,comments')->buildSql();

        $select = DB::name('cwmap_location');
        $res['count'] = $select->where($where)->count();
        $res['da'] = $select->alias('a')
            ->where($where)
            ->join([$subsql => 'b'], 'a.qq = b.bid', 'left')
            ->field('a.id,a.locationname,a.detailaddress,a.longitude,a.latitude,a.picture,a.qq as admin_id,b.comments')
            ->group('a.locationname')
            ->distinct(true)
            ->select();
//halt($res);
        //查询距离的方法调用
        if ($res['count'] > 0) {
            $result = $res['da'];
            foreach ($result as $key => $value) {
                $distance = getDistance($lat, $value['latitude'], $lng, $value['longitude'], 2, 2);
                $result[$key]['distance'] = $distance;
            }
            $res['avg'] = action('comment/avgs');
            $res['da'] = $result;
            $this->success("数据返回成功!", ['indexall' => $res]);
        } else {
            $this->error("暂无数据!");
        }
    }


}
