<?php
/**
 * Notes:
 * User: Administrator
 * Date: 2019/9/2
 * Time: 19:39
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\api\controller;

use think\Db;
use app\common\controller\Api;

class Certificate extends Api
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

       /**
        * 20190922
     * 商家信息查询资质信息查询
     * @param $admin_id     商家用户id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
   public function index($admin_id)
    {
        $findcertifate = Db::name('certificate')
            ->where('admin_id', $admin_id)
            ->find();

        $SafetySupervisorInfo['testResultlist'] = $findcertifate['testResultlist'];
        $SafetySupervisorInfo['conpanyName'] = $findcertifate['conpanyName'];
        $SafetySupervisorInfo['businessAddress'] = $findcertifate['businessAddress'];
        $SafetySupervisorInfo['legalRepresentative'] = $findcertifate['legalRepresentative'];
        $SafetySupervisorInfo['permitNumber'] = $findcertifate['permitNumber'];
        $SafetySupervisorInfo['businessScope'] = $findcertifate['businessScope'];
        $SafetySupervisorInfo['validityPerioddate'] = $findcertifate['validityPerioddate'];
        $SafetySupervisorInfo['id'] = $findcertifate['id'];

        $QualificationInfo['id'] = $findcertifate['id'];
        $QualificationInfo['businessimage'] = $findcertifate['businessimage'];
        $QualificationInfo['foodimage'] = $findcertifate['foodimage'];
        $QualificationInfo['cateringserviceimage'] = $findcertifate['cateringserviceimage'];
        $QualificationInfo['healthimage'] = $findcertifate['healthimage'];
        
        $testResultlist['testResultlist']=$findcertifate['testResultlist'];
        $testResultlist['resultlist']=$findcertifate['resultlist'];
        $manager = $this->manager($admin_id);
        $healthimages = $this->healthimages($admin_id);
		$ilve='Live Kitchen safety grades notification';
        $chufang='厨房直播安全等级公示';
        if (!empty($findcertifate) || $findcertifate == !null) {
            $this->success('ok', ['SafetySupervisorInfo' => $SafetySupervisorInfo,
                'QualificationInfo' => $QualificationInfo,
                'manager' => $manager,
                'healthimages' => $healthimages,
                'testResultlist'=>$testResultlist,
                'live'=>$ilve,'chuf'=>$chufang
            ]);

        } else {
            $this->error('资质信息查询失败！');
        }

    }

    /**
     * 20190922
     * 小程序资质信息管理人员信息接口
     * @param $admin_id
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function manager($admin_id)
    {
        $result = Db::name('worker')
            ->where(['admin_id' => $admin_id, 'state' => 1])
            ->field('id,name,position,mobil,image')
            ->select();
        return $result;
    }

    /**
     * 20190922
     * 小程序资质信息页面健康证接口
     * @param $admin_id
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function healthimages($admin_id)
    {
        $result = Db::name('worker')
            ->where(['admin_id' => $admin_id])
            ->field('id,healthimages')
            ->select();
        return $result;
    }

    /**
     * shangjia信息查询
     * @param $admin_id    商家用户id
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function info($admin_id)
    {
        $info = Db::name('certificate')
            ->where('admin_id', $admin_id)
            ->find();
        $list['logo'] = $info['conpanyLogoavatar'];
        $list['store_name'] = $info['conpanyName'];
        $list['banner'] = $info['bannerimages'];
        return $list;
    }

    /**
     * 分类和菜品图片名称等
     * @param $admin_id   商家用户id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detailed($admin_id)
    {
        $Menu = $this->info($admin_id);
        //查询数据
        $kind = Db::name('kind')->where(['state' => 1,'admin_id'=>$admin_id])->field('id,name')->order('id','asc')->select();

        foreach ($kind as $key => $val) {
            //分类下的食品
            $val['dish'] = Db::name('food')
                ->field('id as aid,food_name,old_price,new_price,posterimage')
                ->where(['admin_id' => $admin_id, 'kind_id' => $val['id']])
                ->select();
            $kind[$key] = $val;
        }

        $this->success("数据返回成功!", ['Menu' => $Menu, 'Kind' => $kind]);
    }

    /**
     * 菜品详情
     * @param $aid  菜品id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function read($aid)
    {
        $select = Db::name('food');
        $result = $select->where(['id' => $aid,'state'=>1])
            ->find();
       // $ingredients['id']=$result['id'];
        $ingredients['name']=$result['food_name'];
         $ingredients['old_price'] = $result['old_price'];
        $ingredients['new_price'] = $result['new_price'];
        $ingredients['poster']=$result['posterimage'];
       // $ingredients['ingredients']=array(\GuzzleHttp\json_decode($result['ingredientjson']));
       $ingredients['ingredients']=array(json_decode($result['ingredientjson']));
        if (!empty($result) || $result == !null) {
            $this->success("数据返回成功!", ['DishDetail' => $ingredients]);
        }
    }

}