<?php
/**
 * 小程序端 视频监控链接
 * Created by PhpStorm.
 * User: kai
 * Date: 2019/11/7
 * Time: 13:29
 */

namespace app\api\controller;

use think\Db;
use app\common\controller\Api;

class Url extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function get_curl($admin_id)
    {
        // $map = ['admin_id' => $admin_id];
        $map = ['admin_id' => 2];

        $select = Db::name('url');
        $result = $select->where($map)
            //->cache(true, 86400 * 7)
            ->select();
        $this->success("数据返回成功!", ['url' => $result]);

    }

}