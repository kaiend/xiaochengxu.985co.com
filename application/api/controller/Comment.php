<?php
/**
 * Notes:评论小程序端接口
 * User: Administrator
 * Date: 2019/9/19
 * Time: 23:22
 * ${PARAM_DOC}
 * @return ${TYPE_HINT}
 * ${THROWS_DOC}
 */

namespace app\api\controller;

use app\common\controller\Api;
use think\Db;
use app\api\model\Comment as cont;

class Comment extends Api
{

    //如果$noNeedLogin为空表示所有接口都需要登录才能请求
    //如果$noNeedRight为空表示所有接口都需要验证权限才能请求
    //如果接口已经设置无需登录,那也就无需鉴权了
    //
    // 无需登录的接口,*表示全部
    // protected $noNeedLogin = ['avgs','read', 'add', 'good_comments', 'good_flag', 'good_hot', 'comment_count', 'count_image'];
    protected $noNeedLogin = ['*'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = ['*'];

    /**
     * 评论页面接口
     * @param $admin_id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
  public function read($admin_id, $order)
    {
        // $order=$this->request->request("order");
        //halt($order);
        $select = Db::name('comment')->alias('a');
        if ($order == 'good_comments') {
            //好评
            $result = $select->where(['admin_id' => $admin_id])
                ->where('delicious', '>', 3)
                ->where('attitude', '>', 3)
                ->where('health', '>', 3)
                ->field('a.id,a.content,a.delicious,a.attitude,a.health,a.createtime,a.commentimages,u.id as uid,u.username,u.nickname,u.avatar')
                ->join('user u', 'a.user_id = u.id')
                ->order('createtime', 'desc')
                ->select();
        } elseif ($order == 'bad_comment') {
            //差评
            $result = $select->where(['admin_id' => $admin_id])
                ->where('delicious', '<', 3)
                ->where('attitude', '<', 3)
                ->where('health', '<', 3)
                ->field('a.id,a.content,a.delicious,a.attitude,a.health,a.createtime,a.commentimages,u.id as uid,u.username,u.nickname,u.avatar')
                ->join('user u', 'a.user_id = u.id')
                ->order('createtime', 'desc')
                ->select();
        } elseif ($order == 'commentimages') {
            //有图
            $result = $select->where(['admin_id' => $admin_id])
                ->where('commentimages', 'not null')
                ->field('a.id,a.content,a.delicious,a.attitude,a.health,a.createtime,a.commentimages,u.id as uid,u.username,u.nickname,u.avatar')
                ->join('user u', 'a.user_id = u.id')
                ->order('createtime', 'desc')
                ->select();
        } else {
            //默认全部并且倒序时间最新
            $select = Db::name('comment')->alias('a');
            $result = $select->where(['admin_id' => $admin_id])
                ->field('a.id,a.content,a.delicious,a.attitude,a.health,a.createtime,a.commentimages,u.id as uid,u.username,u.nickname,u.avatar')
                ->join('user u', 'a.user_id = u.id')
                ->order('createtime', 'desc')
                ->select();
        }
        $comment_conut = $this->comment_count($admin_id);
        $comment_conut['count_images'] = $this->count_image($admin_id);
        $comment_avg = $this->comment_avg($admin_id);
         $interface = Db::name('a')->where('admin_id', $admin_id)->select();
        

        $this->success("数据返回成功!", ['comment' => $result, 'comment_count' => $comment_conut,'comment_avg'=>$comment_avg, 'interface' => $interface]);
    }
    
     /**
     * 计算评论的平均分数
     * @param $admin_id
     * @return mixed
     */
    public function comment_avg($admin_id)
    {
        $comment = new cont();
        $result['good_comments'] =sprintf("%.1f", $comment->where(['admin_id' => $admin_id])
            ->avg('delicious'));
        $result['attitude'] = sprintf("%.1f", $comment->where(['admin_id' => $admin_id])
            ->avg('attitude'));
        $result['health'] = sprintf("%.1f", $comment->where(['admin_id' => $admin_id])
            ->avg('health'));
        $result['avg'] = sprintf("%.1f", ($result['good_comments'] + $result['attitude'] + $result['health']) / 3);

         return $result;
        //$this->success("数据返回成功!", ['comment_avg' => $result]);
    }
    
    /**
     * zai api/index/index/search 调用
     * @param $admin_id
     * @return mixed
     */
    public function avgs($admin_id)
    {
        $comment = new cont();
        $result['good_comments'] = sprintf("%.1f", $comment->where(['admin_id' => $admin_id])
            ->avg('delicious'));
        $result['attitude'] = sprintf("%.1f", $comment->where(['admin_id' => $admin_id])
            ->avg('attitude'));
        $result['health'] = sprintf("%.1f", $comment->where(['admin_id' => $admin_id])
            ->avg('health'));
        $res = sprintf("%.1f", ($result['good_comments'] + $result['attitude'] + $result['health']) / 3);

        return $res;
        //$this->success("数据返回成功!", ['comment_avg' => $result]);
    }
    
	 /**
     * 评论统计
     * @param $admin_id
     * @return mixed
     */
    public function comment_count($admin_id)
    {
        $comment = new cont();
        $result['good_hot'] = $comment->where(['admin_id' => $admin_id])->max('good_hot');
        $result['good_index'] = $comment->where(['admin_id' => $admin_id])->max('good_index');
        $result['good_recommend'] = $comment->where(['admin_id' => $admin_id])->max('good_recommend');
        $result['good_exquisite'] = $comment->where(['admin_id' => $admin_id])->max('good_exquisite');
        $result['good_place'] = $comment->where(['admin_id' => $admin_id])->max('good_place');

        $result['comments'] = $comment->where(['admin_id' => $admin_id])->max('comments');
        $result['good_comments'] = $comment->where(['admin_id' => $admin_id])->max('good_comments');
        $result['bad_comment'] = $comment->where(['admin_id' => $admin_id])->max('bad_comment');
        return $result;

//         $this->success("数据返回成功!", ['comment' => $result]);
    }

    /**
     *评论添加接口
     * @throws \think\Exception
     */
    public function add($admin_id)
    {
        //$add = Db::name('comment');
        $data = $this->request->param();
        $data['createtime'] = time();
        $comment = new cont();
//好评差评统计
        $count = $comment->where(['admin_id' => $admin_id])->count('comments');
        $good_comments = $this->good_comments($admin_id);
        $bad_comments = $this->bad_comment($admin_id);
//flag统计
        $good_hot = $comment->where('flag', '=', 'hot')
            ->count('flag');
        $good_index = $comment->where('flag', '=', 'index')
            ->count('flag');
        $good_recommend = $comment->where('flag', '=', 'recommend')
            ->count('flag');
        $good_exquisite = $comment->where('flag', '=', 'exquisite')
            ->count('flag');
        $good_place = $comment->where('flag', '=', 'place')
            ->count('flag');


        $data['comments'] = $count + 1;
        $data['good_comments'] = $good_comments + 1;
        $data['bad_comment'] = $bad_comments + 1;

        if ($data['flag'] == 'hot') {
            $data['good_hot'] = $good_hot + 1;
        } else {
            $data['good_hot'] = 0;
        }
        if ($data['flag'] == 'index') {
            $data['good_index'] = $good_index + 1;
        } else {
            $data['good_index'] = 0;
        }
        if ($data['flag'] == 'recommend') {
            $data['good_recommend'] = $good_recommend + 1;
        } else {
            $data['good_recommend'] = 0;
        }
        if ($data['flag'] == 'exquisite') {
            $data['good_exquisite'] = $good_exquisite + 1;
        } else {
            $data['good_exquisite'] = 0;
        }
        if ($data['flag'] == 'place') {
            $data['good_place'] = $good_place + 1;
        } else {
            $data['good_place'] = 0;
        }


        $result = $comment->save($data);
        if ($result) {
            $this->success("评论成功!");
        } else {
            $this->error(__('评论失败'));
        }
    }

    /**
     * 好评数量
     * @return int|string
     * @throws \think\Exception
     */
    public function good_comments($admin_id)
    {
        $comment = new cont();
        $good_count = $comment
            ->where(['admin_id' => $admin_id])
            ->where('delicious', '>', 3)
            ->where('attitude', '>', 3)
            ->where('health', '>', 3)
            ->count('comments');
        return $good_count;
    }

    /**
     * 差评数量统计
     * @return int|string
     * @throws \think\Exception
     */
    public function bad_comment($admin_id)
    {
        $comment = new cont();
        $bad_count = $comment
            ->where(['admin_id' => $admin_id])
            ->where('delicious', '<', 3)
            ->where('attitude', '<', 3)
            ->where('health', '<', 3)
            ->count('comments');
        return $bad_count;
    }

    public function count_image($admin_id)
    {
        $comment = new cont();
        $count_image = $comment
            ->where(['admin_id' => $admin_id])
            ->where('commentimages', 'not null')
            ->count('commentimages');
        return $count_image;
    }


}